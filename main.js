(function ($) {

    var $mainWrap = $('.main-wrap'),
        to1 = null,
        scrollToPage = null,
        isMobile = false,
        noTransit = false,
        isRTL;

    if (!$.support.transition) {
        $.fn.transition = $.fn.animate;
        noTransit = true;
    }
    $(window).load(function () {
        $(".loader").fadeOut(1000);
    });
    var _dir = 1;
    if (isRTL) _dir = -1;
    window.onresize = function () {
        location.reload();
    };
    // mobile / desktop switch
    isHome();
    $(window).on('load', function () {

        $('.his-side-page, .vis-side-page').fadeOut();



        var _w = $(this).width();
        if (_w <= 750) {
            $('body').removeClass('desktop').addClass('mobile');
            isMobile = true;
        } else {
            $('body').removeClass('mobile').addClass('desktop');
            initDesktop();
        }
    });

    $(window).on('resize', function () {
        var _w = $(this).width();
        if (_w <= 767 && $('body').is('.desktop')) location.reload();
        if (_w > 767 && $('body').is('.mobile')) location.reload();
    });

    // initialize Desktop
    function initDesktop() {
        // determine screen ratio
        var _screenRatio = 1;

        function setScreenRatio() {
            var $w = $(this);
            _screenRatio = $w.width() / $w.height();
            // console.log('ratio:' + _screenRatio);
            if (_screenRatio > 1.5) $('body').addClass('wide');
            else $('body').removeClass('wide');
            if (_screenRatio >= 1.9) $('body').addClass('extra-wide');
            else $('body').removeClass('extra-wide');
        }
        setScreenRatio();
        $(window).on('resize', function () {
            setScreenRatio();
        });

        isHome(); //check if top of page

        function applyScrollBar() {
            $('.current-page').not('#media').find('.thecopy').each(function () {
                var $me = $(this),
                    $p = $me.parent();
                var $scrollable = $('<div class="scrollable" />');

                if ($me.find('.scrollable').length) {
                    $me.find('.scrollable').jScrollPane().data().jsp.destroy();
                } else $me.wrapInner($scrollable);
                $me.find('.scrollable').height($p.height());
                $me.find('.scrollable').off('mousewheel').on('mousewheel', function () {
                    if ($(this).is('.jspScrollable')) return false;
                }).jScrollPane({
                    autoReinitialise: true,
                    verticalGutter: 10,
                    verticalDragMaxHeight: 130,
                    hideFocus: true
                }).data().jsp.scrollToY(0);
            });
        }

        // mousewheell scrolling
        $('.pages-wrap').off('mousewheel').on('mousewheel', function (e) {


            if ($mainWrap.is('.locked') || e.deltaFactor < 2) {
                e.preventDefault();
                return false;
            }

            /*	$mainWrap.addClass('locked');*/


            if (e.deltaY < 0) {
                $('.bullets a.active').next().click();
                if ($('#media').is('.current-page')) {
                    var _dropEffect = 'cubic-bezier(.52,.22,.35,1.8)';
                    if (noTransit) _dropEffect = 'easeInExpo';
                    $('#media').stop().transition({
                        y: -100
                    }, 300, 'easeOutQuad').transition({
                        y: 0
                    }, 200, _dropEffect);
                }
            } else {
                $('.bullets a.active').prev().click();
                if ($('#home').is('.current-page')) {
                    var _dropEffect = 'cubic-bezier(.52,.22,.35,1.8)';
                    if (noTransit) _dropEffect = 'easeInExpo';
                    $('#home').stop().transition({
                        y: 75
                    }, 300, 'easeOutQuad').transition({
                        y: 0
                    }, 200, _dropEffect);
                }
            }

            to1 = setTimeout(function () {
                if ($('.current-page').is('#contact')) $('.scroll-hint').addClass('go-top');
                if ($('.history-wrap').is(':visible')) {
                    $('.scroll-hint').fadeOut();
                }
                $mainWrap.removeClass('locked');

            }, 1200);

        });

        // keyboard scrolling
        $(document).on('keyup', function (e) {
            if ($mainWrap.is('.locked')) return false;
            if (e.which == 40 || e.which == 34) $('.bullets a.active').next().click();
            if (e.which == 38 || e.which == 33) $('.bullets a.active').prev().click();
        });


        // pages
        $('.pages-wrap').each(function () {
            var $me = $(this),
                $page = $me.find('> .page');
            var to2 = null;
            var isIpad = navigator.userAgent.match(/iPad;.*CPU.*OS 7_\d/i) && !window.navigator.standalone;

            function adjustBoxHeight() {
                var _h = 0;
                var $win = $(window);
                var $current = $('.current-page');
                var _fix = 0;
                if (isIpad && $win.width() == 1024) _fix = 20; // fix for iPad landscape issue
                $page.each(function () {
                    var $t = $(this);
                    $t.height($win.height() - _fix);
                    _h += $t.height();
                });

                $me.height(_h);

                // adjust position of current page
                clearTimeout(to2);
                to2 = setTimeout(function () {
                    if ($current.length) scrollToPage($current, 200);
                }, 300);
            }
            adjustBoxHeight();

            $(window).on('resize', function () {
                adjustBoxHeight();
            });

        });

        function animatePage($page) {
            $('.main-logo').css({
                "display": "block",
                "opacity": "1",
                "visibility": "visible"
            });
            if ($page.is('#home')) {
                $('.main-logo').css({
                    "display": "none",
                    "opacity": "0",
                    "visibility": "hidden"
                });



            }
            if ($page.is('#offer')) {

                $(".overlay").removeClass('pt-page-scaleDown');
                $(".test1 .itemp-parrent p").hide();

                $(".test1 .itemp-parrent").removeClass('pt-page-scaleUp');
                setTimeout(function () {
                    $(".overlay").addClass('pt-page-scaleDown');

                }, 1000);
                setTimeout(function () {
                    $(".test1 .itemp-parrent").addClass('pt-page-scaleUp');
                }, 200);
                setTimeout(function () {
                    $(".test1 .itemp-parrent p").show();
                }, 2500);

            }

            $page.find('.side-menu, .dummy-box').css({
                x: _dir * -400
            }).delay(1800).transition({
                x: 0
            }, 200, 'easeOutCubic');
            /*	$page.find('.side-cta').css({x: _dir*400}).delay(2000).transition({x: 0}, 200,'easeOutCubic');*/
            $page.find('.side-thumb').css({
                x: (_dir * 100) + '%'
            }).delay(2000).transition({
                x: 0
            }, 200, 'easeOutCubic');

            if ($page.is('#impact')) {
                $page.find('.impact-wrap').css({
                    y: '100%'
                }).delay(400).transition({
                    y: 0
                }, 800, 'easeOutCubic');
                // do boxes animation
                $page.find('.boxes-wrap .cta-box').each(function (i) {
                    var $t = $(this);
                    $t.css({
                        x: _dir * 100,
                        opacity: 0
                    }).delay(1200 + i * 150).transition({
                        opacity: 1,
                        x: 0
                    }, 100 + 200 * i);
                });

            }

            if ($page.is('#who_we_are')) {

                $(".test1 .itemp-parrent p").hide();
                $('.content').css({
                    opacity: 0
                });
                setTimeout(function () {
                    $('.content').css({
                        y: -210,
                        opacity: 0
                    }).delay().transition({
                        opacity: 1,
                        y: -180
                    }, 1500);
                }, 1000);

            }
            if ($page.is('#home1')) {
                var vid2 = document.getElementById("example_video_2");
                vid2.autoplay = true;
                vid2.load();


                $('.content').css({
                    opacity: 0
                });
                setTimeout(function () {
                    $('.content').css({
                        y: -80,
                        opacity: 0
                    }).delay().transition({
                        opacity: 1,
                        y: -50
                    }, 1500);
                }, 1300);

            }
            if ($page.is('#ethics')) {
                $('.content').css({
                    opacity: 0
                });
                setTimeout(function () {
                    $('.content').css({
                        y: -210,
                        opacity: 0
                    }).delay().transition({
                        opacity: 1,
                        y: -180
                    }, 1500);
                }, 1000);
            }
            if ($page.is('#contact')) {
                $('.scroll-hint').addClass('go-top');

                $('.main-logo').css({
                    "display": "none",
                    "opacity": "0",
                    "visibility": "hidden"
                });

                $('.contact-sec').eq(0).css({
                    x: '-100%',
                    opacity: 0.5
                }).delay(200).transition({
                    opacity: 1,
                    x: 0
                }, 1000, 'easeOutCubic');

            }
            if ($page.is('#press')) {


                $(".overlay1").removeClass('pt-page-scaleDown');
                $("#press .spot-item h3").hide();
                $(".test1 .spot-item").removeClass('pt-page-scaleUp');
                setTimeout(function () {
                    $(".overlay1").addClass('pt-page-scaleDown');

                }, 1000);
                setTimeout(function () {
                    $('#press .item-parrent').eq(0).css({
                        x: '-100%',
                        opacity: 0.5
                    }).delay(200).transition({
                        opacity: 1,
                        x: 0
                    }, 1000, 'easeOutCubic');
                    $('#press .item-parrent').eq(1).css({
                        x: '100%',
                        opacity: 0.5
                    }).delay(200).transition({
                        opacity: 1,
                        x: 0
                    }, 1000, 'easeOutCubic');
                }, 600);
                setTimeout(function () {
                    $("#press .spot-item h3").show();
                }, 2500);




            }
        }


        scrollToPage = function ($page, _speed) {
            var _y = $page.position().top,
                _currentTop = $('.current-page').length ? $('.current-page').position().top : 0;
            if (!_speed) _speed = 1600;

            var _delta = (Math.abs(_y - _currentTop) / $page.height()) * 300;

            if (_speed == 200) {} else {
                setTimeout(function () {
                    animatePage($page);
                }, _delta - 300);
            }


            resetSubpage($page);

            // close history
            if ($('.history-wrap').is(':visible')) closeHistory(true);

            // hide contact form
            $('.contact-form-wrap:visible .close').click();

            // hide T and C / Privacy Policy
            $('.overlay3 .close').click();

            // now scroll to page
            $('.pages-wrap').stop().transition({
                x: 0,
                y: _y * -1
            }, _delta + _speed, 'easeOutCubic', function () {
                isHome();
                $('.page').removeClass('current-page');
                $page.addClass('current-page');

                if (!$page.is('#contact')) $('.scroll-hint').removeClass('go-top');
                $mainWrap.removeClass('locked');

                // lock scroll if subpage is visible
                if (parseInt($page.find('.subpage-wrap').css('x')) < 0) {
                    $mainWrap.addClass('locked');
                }

                // apply scrollbar
                applyScrollBar();
            });

            // hide main menu
            if ($('.main-menu').is('.main-menu-expanded')) $('.main-menu .cta')[0].click();
        };
        // scrollToPage($('.page').eq(6));

        function gotoThePage($target) {

            if ($target.length) {
                scrollToPage($target);

                // set active bullet
                $('.bullets a').removeClass('active').filter('a[href=#' + $target.attr('id') + ']').addClass('active');

                // hide scroll down hint
                if ($target.is('#contact')) $('.scroll-hint').addClass('go-top');
                else $('.scroll-hint').removeClass('go-top');
            }

        }

        // deep linking
        $.address.change(function (e) {
            var _hash = e.value;
            var _anchor = '#' + _hash.replace('/', '');
            var $target = $(_anchor);
            gotoThePage($target);
            $('.themenu li').removeClass('active').find('a[href=' + _anchor + ']').parent().addClass('active');
        });
        // scroll to hashed page on load
        var _hash = window.location.hash;
        if (_hash !== '') {
            var _anchor = _hash.replace('/', '');
            var $target = $(_anchor);
            gotoThePage($target);
            $('.themenu li').removeClass('active').find('a[href=' + _anchor + ']').parent().addClass('active');
        }




        function resetSubpage($page) {
            $page.find('.subpage-wrap, .copy-wrap').css({
                x: 0,
                opacity: 1
            });
        }

        // bullets & main menu links when click
        $('.bullets a, .header h1 a, .themenu a').on('click', function (e) {
            if ($(this).is('[target=_blank]')) return true;
            $.address.value($(this).attr('href').replace('#', ''));
            e.preventDefault();
            return false;
        });


        // spotlight section
        $('.page-offer .spot-item').each(function () {
            var $t = $(this),
                _img = $t.find('.keyimage > img').attr('src');
            $t.css({
                background: 'url(' + _img + ') 50% 0% no-repeat',
                backgroundSize: 'cover'
            });

        });






        // adjust height of square boxes
        $('.side-cta, .side-menu').each(function () {
            var $t = $(this);

            function adjustHeight() {
                $t.height($t.width());
                $t.parent().outerHeight($t.outerHeight() * 2);
            }
            adjustHeight();
            $(window).on('resize', function () {
                adjustHeight();
            });

        });

        // scroll hint click
        $('.scroll-hint').on('click', function () {
            var $t = $(this);
            if ($t.is('.go-top')) $('.bullets a:eq(0)').click();
            else $('.bullets a.active').next().click();
        });









        // Our Group Section
        $('#who_we_are .subpage-wrap').each(function () {
            var $me = $(this),
                $copyWrap = $me.prev('.copy-wrap'),
                $cta = $copyWrap.find('.side-cta'),
                $cta1 = $copyWrap.find('.side-cta1');
            var $subpage = $me.find('.subpage');
            var he = $('#who_we_are ').height();

            $subpage.css("height", he);

            var $boxes = $me.find('.subpage-boxes');
            var $detail = $me.find('.subpage-detail');

            // create dummy box
            $cta.after('<div class="dummy-box" />');

            // side-cta on click
            $cta.on('click', function () {
                $('.vis-side-page .wwr').css({
                    opacity: 0
                });
                setTimeout(function () {
                    $('.vis-side-page .wwr').css({
                        y: -250,
                        opacity: 0
                    }).delay().transition({
                        opacity: 1,
                        y: -200
                    }, 1500);
                }, 200);
                /*var $t = $(this), _w = $(window).width();*/
                $('.wwr-content, .side-cta, .side-cta1').fadeOut(100);
                $mainWrap.addClass('locked');
                $('.vis-side-page').fadeIn(200);


            });



            $cta1.on('click', function () {
                $('.his-side-page .wwr').css({
                    opacity: 0
                });
                setTimeout(function () {
                    $('.his-side-page .wwr').css({
                        y: -300,
                        opacity: 0
                    }).delay().transition({
                        opacity: 1,
                        y: -250
                    }, 1500);
                }, 200);
                $('.wwr-content, .side-cta, .side-cta1').fadeOut(100);
                /*	var $t = $(this), _w = $(window).width();*/

                $mainWrap.addClass('locked');

                $('.his-side-page').fadeIn(200);


            });



            $subpage.each(function () {
                var $t = $(this),
                    $back = $t.find('.back'),
                    $back1 = $t.find('.back1');

                $back.on('click', function () {
                    $('.vis-side-page').fadeOut(100);
                    $('.wwr-content, .side-cta, .side-cta1').fadeIn(200);
                    setTimeout(function () {
                        $mainWrap.removeClass('locked');
                    }, 500);

                });

                $back1.on('click', function () {
                    $('.his-side-page').fadeOut(100);
                    $('.wwr-content, .side-cta, .side-cta1').fadeIn(200);
                    setTimeout(function () {
                        $mainWrap.removeClass('locked');
                    }, 500);


                });

                if ($t.is($boxes)) {
                    // when boxes are clicked
                    $t.find('.box').not('.box-history').on('click', function () {
                        var _w = $(window).width();
                        var $b = $(this),
                            i = $t.find('.box').index($b);
                        $me.transition({
                            x: '-=' + (_dir * _w) + 'px'
                        }, 800);

                        // do boxes animation
                        $me.find('.subpage-detail .boxes-wrap2 .box').each(function (i) {
                            var $t = $(this);
                            $t.css({
                                y: '100%'
                            }).delay(300 + i * 200).transition({
                                y: 0
                            }, 300, 'easeOutCubic');
                        });
                        $me.find('.subpage-detail .back').css({
                            opacity: 0,
                            x: (_dir * 50) + '%'
                        }).delay(1000).transition({
                            opacity: 1,
                            x: 0
                        });
                        $me.find('.subpage-detail .back1').css({
                            opacity: 0,
                            x: (_dir * 50) + '%'
                        }).delay(1000).transition({
                            opacity: 1,
                            x: 0
                        });

                        // set active menu & content
                        $detail.find('.side-menu2 li').removeClass('active').eq(i).addClass('active');
                        $detail.find('.box-content-item').hide().removeClass('active').eq(i).addClass('active').show();

                        //applyScrollBar2($detail.find('.box-content'));

                        //populateThumbnails($detail);

                        return false;
                    });

                    /*	$t.find('.box-history').on('click', function(){
                    		loadHistory();
                    		return false;
                    	});*/
                }
            });



        });




    } //end of desktop


    $('.loader-page').fadeOut(500);

    // main menu expand/collapse
    $('.main-menu').each(function () {
        var $me = $(this),
            $cta = $me.find('.cta');
        var $util = $('.header').find('.util');

        var _origin = $me.position().left;
        if (isRTL) _origin = parseInt($me.css('right'));
        $me.data('origin', _origin);


        $cta.on('click', function () {
            var $h1 = $me.prev('h1');


            if (!$me.is('.main-menu-expanded')) {
                if (isRTL) $me.transition({
                    right: 0
                });
                else $me.transition({
                    left: 0
                });

            } else {
                if (isRTL) $me.transition({
                    right: _origin
                });
                else $me.transition({
                    left: _origin
                });
            }

            $me.toggleClass('main-menu-expanded');

            if ($me.is('.main-menu-expanded')) $h1.css({
                top: 0
            });
            else $h1.removeAttr('style');

            if (isMobile) {
                if ($me.is('.main-menu-expanded')) {
                    var _top = $h1.outerHeight() + $me.find('.themenu').outerHeight();
                    $util.css({
                        top: _top
                    });
                } else {
                    $util.removeAttr('style');
                }
            }

            return false;
        });

        $('body').on('click', function () {
            if ($me.is('.main-menu-expanded')) $cta.click();
        });
        $me.on('click', function (e) {
            if ($(e.target).is('a[target=_blank]')) return true;
            e.preventDefault();
            e.stopPropagation();
            return false;
        });
    });

    // is home
    function isHome() {
        if (parseInt($('.pages-wrap').css('y')) >= 0) {

            $mainWrap.addClass('is-home');
            return true;
        } else {
            $mainWrap.removeClass('is-home');
            return false;
        }
    }


    // home slogan
    $('.home-slogan .slogan strong').fitText(1.9, {
        maxFontSize: '32px'
    });


    // adjust height of video player
    $('.video-wrap').each(function () {
        var $me = $(this),
            $close = $me.find('.close'),
            $iframe = $me.find('iframe');

        function adjustHeight() {
            $me.height($me.width() * (540 / 960));
        }
        // adjustHeight();
        $(window).on('load resize', function () {
            adjustHeight();
        });

        $close.on('click', function () {
            $('.overlay').fadeOut(function () {
                $iframe.attr('src', $iframe.attr('src'));
            });
        });
        // home slogan video CTA
        $('.home-slogan .cta').on('click', function () {
            $('.overlay').fadeIn();
            adjustHeight();
            return false;
        });
    });



    // news list
    $('.news-list').each(function () {
        var $me = $(this).addClass('clearfix'),
            $li = $me.find('> ul > li'),
            $links = $me.find('li a.more'),
            $articleWrap = $('.article-wrap');

        $links.on('click', function () {
            var $t = $(this),
                id = $t.data('target');
            if (!id) return;

            $articleWrap.find('.article-detail').css({
                display: 'none'
            }).removeClass('current').filter('[data-article-id=' + id + ']').addClass('current').css({
                x: 0,
                display: 'block'
            });

            return false;
        });



        // list paging
        $(window).on('load', function () {

            if (!isMobile) {

                var $pager = $('<div class="pager" />');
                var _visible = 4;
                if ($me.parent().is('.news-list-wrap')) _visible = 6;
                var _pageCount = Math.ceil($li.length / _visible);
                for (var i = 0; i < _pageCount; i++) {
                    if (i === 0) $pager.append('<span data-target="grp' + i + '" class="active">' + (i + 1) + '</span>');
                    else $pager.append('<span data-target="grp' + i + '">' + (i + 1) + '</span>');
                }
                $li.each(function (i) {
                    var _x = Math.floor(i / _visible);
                    $(this).addClass('grp' + _x);
                });

                $pager.on('click', 'span', function () {
                    var $t = $(this),
                        i = $pager.find('span').index($t),
                        _target = $t.data('target');
                    if ($t.is('.active')) return false;
                    var currIndex = $pager.find('span').index($pager.find('.active'));
                    var dir = i > currIndex ? (_dir * 200) + '%' : (_dir * -200) + '%';
                    $li.css({
                        display: 'none'
                    }).filter('.' + _target).css({
                        x: dir,
                        opacity: 0,
                        display: 'block'
                    }).delay(50).transition({
                        x: 0,
                        opacity: 1
                    }, 500);
                    $pager.find('span').removeClass('active');
                    $(this).addClass('active');
                    // set scroll to Top
                    $me.data('jsp').scrollToY(0);
                    return false;
                });

                $me.before($pager);


                if ($me.parent().is('.news-list-wrap')) {

                    $(window).on('resize', function () {

                    });

                    // invoke scrollbar here...
                    $me.off('mousewheel').on('mousewheel', function () {
                            if ($(this).is('.jspScrollable')) return false;
                        })
                        .jScrollPane({
                            autoReinitialise: true,
                            verticalGutter: 10,
                            verticalDragMaxHeight: 130,
                            hideFocus: true
                        });
                }

            }
        });
    });


    // load history - defined below inside history wrap closure
    var closeHistory = function () {};

    // history
    $('.history-wrap').each(function () {
        var $me = $(this),
            $theList = $me.find('.history-list'),
            $items = $me.find('.history-item'),
            $hvids = $me.find('.history-vid'),
            $timelist = $me.find('.time-list');


        // load images/ thumbnails
        var $thumbWraps = $me.find('.thumb2, .video-thumb');


        $(window).on('resize', function () {
            adjustThumbsHeight();
            setTheListHeight();
            adjustTimeList();
        });

        function adjustThumbsHeight() {
            if (isMobile) {
                $me.find('.thumb1').each(function () {
                    $(this).height($(this).width());
                });
            }
            $thumbWraps.each(function () {
                $(this).height($(this).width());
            });
        }

        function setTheListHeight() {
            var _len = $theList.find('> li').length;
            var _h = $(window).height();
            $theList.find('> li').height(_h);
            $theList.height(_len * _h);
        }
        adjustThumbsHeight();
        setTheListHeight();

        // generate timeline
        $hvids.each(function (i) {
            var $t = $(this);
            var $mod = $t.find('.offermodule'),
                $close = $t.find('.back, .close');

            $close.on('click', function () {

                $mod.removeClass('pt-page-scaleUp');
                $items.removeClass('pt-page-scaleUp');
                $mod.addClass('pt-page-scaleDownOffer');
                $items.addClass('pt-page-scaleDownOffer');


                setTimeout(function () {
                    $mainWrap.removeClass('locked');
                    $mainWrap.removeClass('offer-locked');
                }, 800);


                return false;
            });

            if (i % 2 && isMobile) $t.parent().addClass('odd');
        });

        function adjustTimeList() {
            $timelist.css({
                marginTop: $('.header h1').height() + 30
            });
            $timelist.find('>li').height($(window).height() * 0.054);
        }
        adjustTimeList();

        // time line click
        $timelist.find('> li').on('click', function () {
            $mainWrap.addClass('locked');
            $mainWrap.addClass('offer-locked');
            var $t = $(this),
                i = $timelist.find('> li').index($t);
            scrollToHistory($items.eq(i).parent(), function () {
                $timelist.find('> li').removeClass('active');
                $t.addClass('active');
                $items.eq(i).addClass('current');
            });
            return false;
        });

        $(".ofr_listing>a").on('click', function () {
            $mainWrap.addClass('locked');
            $mainWrap.addClass('offer-locked');
            var $ofr = $('.ofr_listing');
            var $t = $(this),
                i = $ofr.find('> a').index($t);
            scrollToHistory($items.eq(i).parent(), function () {
                $ofr.find('> a').removeClass('active');
                $t.addClass('active');
                $items.eq(i).addClass('current');
            });
            return false;
        });

        function scrollToHistory($page, fn) {
            var _yTarget = $page.position().top;
            var _pctDelta = (_yTarget / $page.parent().height() * 100).toFixed(2);
            var _currentTop = Math.abs(parseInt($theList.css('y')));
            var dir = _currentTop < _yTarget ? '' : '-';

            $theList.transition({
                y: _yTarget * -1
            }, 1200, 'easeOutCubic');
            if (fn) fn();

            animateHistoryPage($page, dir);

            if ($timelist.find('> li:last-child').is('.active')) $('.scroll-hint2').addClass('go-top');
            else $('.scroll-hint2').removeClass('go-top');

            // adjust timeline ruler view
            if ($timelist.outerHeight() > $timelist.parent().height()) $timelist.transition({
                y: (_pctDelta / 2).toFixed(2) * -1 + '%'
            }, 1200);
        }

        function animateHistoryPage($page) {

            $me.find('.bg-image, .history-copy, .thumb1, .video-thumb, .thumb2, .back').stop();


            $page.find('.offermodule, .history-item').removeClass('pt-page-scaleDownOffer');
            $page.find('.offermodule').addClass('pt-page-scaleUp');

            $page.find('.history-item').addClass('pt-page-scaleUp');


            $page.find('.back').css({
                y: '150%',
                opacity: 0
            }).delay(1500).transition({
                y: 0,
                opacity: 1
            }, 500);
        }

        closeHistory = function () {

        };

        // mousewheel scroll
        $me.off('mousewheel').on('mousewheel', function (e) {
            if ($me.is('.locked')) {
                e.preventDefault();
                return false;
            }

            /*	$me.addClass('locked'); */
            if (e.deltaY < 0) {
                $timelist.find('li.active').next().click();
            } else {
                $timelist.find('li.active').prev().click();
            }
            setTimeout(function () {
                $me.removeClass('locked');
            }, 2000);
        });


        $('.scroll-hint2').on('click', function () {
            var $t = $(this);
            if ($t.is('.go-top')) {
                $timelist.find('li:eq(0)').click();
                return false;
            }
            $timelist.find('li.active').next().click();
            return false;
        });

        var to = null;
        var $videoBg = $('.video-bg');
        var $video = $('.video-bg video');

        var $w = $(this);
        clearTimeout(to);
        to = setTimeout(function () {
            var ratio = (960 / 540);
            if ($videoBg.width() / $videoBg.height() >= ratio) {
                $videoBg.height('100%');
                $video.height($w.width() / ratio);
                $video.width($w.width());
            } else {
                // $videoBg.width($videoBg.height() * ratio);
                $video.height($w.height());
                $video.width($w.height() * ratio);
            }
        }, 400);

    });


    // apply class names to video containers
    $('.box-content-item div > iframe').each(function () {
        var $t = $(this),
            $div = $t.parent();

        $div.addClass('video-container');
    });

    /**************magical line on hover*****************/
    var $el, leftPos, newWidth;
    $("#example-one").append("<a id='magic-line'></a>");
    var $magicLine = $("#magic-line");

    $magicLine
        .width($(".current_page_item").width())
        .css("left", $(".current_page_item").position().left)
        .data("origLeft", $magicLine.position().left)
        .data("origWidth", $magicLine.width());

    $("#example-one a").find("li").hover(function () {
        $el = $(this);
        leftPos = $el.position().left;
        newWidth = $el.width();

        $magicLine.stop().animate({
            left: leftPos,
            width: newWidth
        });
    }, function () {
        $magicLine.stop().animate({
            left: $magicLine.data("origLeft"),
            width: $magicLine.data("origWidth")
        });
    });
    /**************Spot-item on hover*****************/

    $("#offer .spot-item").css("opacity", 1.0);
    $("#offer .spot-item").hover(function () {

            $("#offer .spot-item").not(this).stop().animate({
                opacity: 0.35
            }, "medium");
            /*$(".test1").css("background","black");*/
        },
        function () {
            $("#offer .spot-item").stop().animate({
                opacity: 1.0
            }, "medium");
            /*  $(".test1").css("background","transparent");   */
        });


    $("#press .spot-item").css("opacity", 1.0);
    $("#press .spot-item").hover(function () {

            $("#press .spot-item").not(this).stop().animate({
                opacity: 0.5
            }, "medium");
            $(".press").css("background", "black");
        },
        function () {
            $("#press .spot-item").stop().animate({
                opacity: 1.0
            }, "medium");

        });


    /**************audio*****************/
    /* $("#mute").click(function(){
	 $("#example_video_1").prop('muted', true);
 });

 $("#mute").on('click', muteVid)

function muteVid() {
    
	 $("#example_video_1").prop('muted', true);
    $("#mute").off('click').on('click', unMuteVid);
	 $("#mute").attr('src', 'assets/img/unmute.jpg');
}

function unMuteVid() {
   
	 $("#example_video_1").prop('muted', false);
    $("#mute").off('click').on('click', muteVid);
	$("#mute").attr('src', 'assets/img/mute.jpg');
} */

    var vid = document.getElementById("example_video_1");
    var vid2 = document.getElementById("example_video_2");
    vid.autoplay = true;
    vid.load();
    vid2.autoplay = true;
    vid2.load();

    /*$("#first").click(function(){

	 vid2.autoplay = true;
    vid2.load();
 });
 */

    /*function pc(){*/

    var w = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;

    if (w < 1024) {
        $('.menu-section').unbind('mouseenter mouseleave');
        tab();

    } else {
        $('.menu-section').unbind('mouseenter mouseleave');
        pc();

    }


    function pc() {
        $('.menu-section').bind('mouseenter mouseleave', function (event) {
            switch (event.type) {
            case 'mouseenter':


                /*     $(".menu").stop().slideDown(500);*/
                $(".menu").stop().animate({
                    top: '0'
                }, 'slow');

                $(" #one span").stop().fadeTo('slow', 0);
                break;
            case 'mouseleave':

                /*  $(".menu").stop().slideUp(500);*/
                $(".menu").stop().animate({
                    top: '-108px'
                }, 'slow');

                $("#one img, #one span").stop().fadeTo(500, 1);
                break;
            }
        });
    }

    function tab() {

        $('.menu-section').bind('mouseenter mouseleave', function (event) {
            switch (event.type) {
            case 'mouseenter':
                $(".menu").stop().animate({
                    top: '0'
                }, 'slow');

                $(" #one img,#one span").stop().fadeTo('slow', 0);
                break;
            case 'mouseleave':
                $(".menu").stop().animate({
                    top: '-108px'
                }, 'slow');

                $("#one img, #one span").stop().fadeTo(500, 1);
                break;
            }
        });
    }

    /*----------------press sub items --------------------*/

    $('#press .subpage-wrap').each(function () {
        var $me = $(this);
        var he = $('#press ').height();

        $me.css({
            "height": he,
            'opacity': 0
        });

    });
    $('#contact .subpage-wrap').each(function () {

        var $me = $(this);
        var $subpage = $me.find('.subpage');
        var he = $('#contact').height();

        $me.css({
            "height": he,
            'opacity': 0
        });
        $subpage.css({
            "height": he,
            'opacity': 1,
            'left': 0
        });

    });
    $("#news").click(function () {

        $('.newsPage').removeClass('news-page-scaleDown');
        $('.newsPage').addClass('news-page-scaleUp');
        $(".main-wrap").addClass('locked');

    });
    $(".backNews").click(function () {
        $('.newsPage').removeClass('news-page-scaleUp');
        $('.newsPage').addClass('news-page-scaleDown');



        $(".main-wrap").removeClass('locked');
        /*$('.newsPage').removeClass('news-page-scaleUp');
         */

    });
    $("#anns").click(function () {

        $('.annsPage').removeClass('news-page-scaleDown');
        $('.annsPage').addClass('news-page-scaleUp');
        $(".main-wrap").addClass('locked');

    });
    $(".backAnns").click(function () {
        $('.annsPage').removeClass('news-page-scaleUp');
        $(".main-wrap").removeClass('locked');
    });
    $(".vw_map").click(function () {

        $('.mapPage').removeClass('news-page-scaleDown');
        $('.mapPage').addClass('news-page-scaleUp');
        $(".main-wrap").addClass('locked');
        google.maps.event.trigger(map, 'resize');

    });
    $(".backMap").click(function () {
        $('.mapPage').removeClass('news-page-scaleUp');
        $(".main-wrap").removeClass('locked');
    });
    $('.select-box').hide();
    $('.select1').click(function () {
        $('.select-box').toggle();
    });
    /*------------------------------roll over image----------------*/
    $(".roll").css("opacity", "0"); // ON MOUSE OVER
    $(".roll").hover(function () {
            // SET OPACITY TO 70%
            $(this).stop().animate({
                opacity: 0.7
            }, "slow");
        },
        // ON MOUSE OUT
        function () {
            // SET OPACITY BACK TO 50%
            $(this).stop().animate({
                opacity: 0
            }, "slow");
        });



    $('.roll').click(function () {

        var thumb = $(this).next('.thumbnail1');
        var address = thumb.attr('src');
        $('#popup').fadeIn('slow');
        $('#press .row .col-md-2, .backNews').css("opacity", 0.3);
        $('#lightbox').attr('src', address);
    });
    $('#close').click(function () {
        $('#popup').fadeOut('fast');
        $('#press .row .col-md-2, .backNews').css("opacity", 1);
    });


    // Form

    function std_email_form(obj) {
        var hasError = false;
        obj.find('.required').each(function () {
            if (jQuery.trim($(this).val()) === '' || $(this).val() === '0') {
                hasError = true;
                $(this).addClass('invalid');
            } else {
                $(this).removeClass('invalid');
            }
        });
        var invalid = obj.find('.submit-invalid');
        invalid.hide();
        if (!hasError) {

            invalid.hide();
        } else {
            invalid.hide().html('Please fill in the required fields').fadeIn(200);

        }

        return false;
    }
    $('.select-box a').click(function (e) {
        e.preventDefault();
    });

    $('#myForm').submit(function (e) {
        e.preventDefault();
        std_email_form($(this), '<div class="success-message"><p>Thank you for your message!</p></div>');
    });

    $('#myForm .select-box a').click(function () {
        var val = $(this).text();
        $('.default-value').text(val);
    });
})(jQuery);